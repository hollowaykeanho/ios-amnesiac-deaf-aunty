//
//  RootViewController.m
//  AmnesiacDeafAunty
//
//  Created by Kean Ho Chew on 28/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import "RootViewController.h"
#import "EnterRepetitionViewController.h"

@interface RootViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *speechTextField;
@property (weak, nonatomic) IBOutlet UIButton *sayItButton;


- (IBAction)editingChanged:(id)sender;

@end




@implementation RootViewController

- (IBAction)unwindToRootViewController:(UIStoryboardSegue *)sender
{
    self.speechTextField.text = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.speechTextField.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([self.speechTextField.text length] != 0) {
        return YES;
    } else {
        return NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SayIt"]) {
        EnterRepetitionViewController *enterRepetitionViewController = [segue destinationViewController];
        
        enterRepetitionViewController.speech = self.speechTextField.text;
    }
}

- (IBAction)editingChanged:(id)sender
{
    if ([self.speechTextField.text length] != 0) {
        self.sayItButton.enabled = YES;
    } else {
        self.sayItButton.enabled = NO;
    }
}
@end
