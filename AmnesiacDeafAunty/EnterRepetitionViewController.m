//
//  EnterRepetitionViewController.m
//  AmnesiacDeafAunty
//
//  Created by Kean Ho Chew on 28/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#include <stdlib.h>
#import "EnterRepetitionViewController.h"
#import "ResultViewController.h"

@interface EnterRepetitionViewController ()

@property (weak, nonatomic) IBOutlet UILabel *responseLabel;
@property (weak, nonatomic) IBOutlet UITextField *timeToRepeatField;
@property (weak, nonatomic) IBOutlet UIButton *seeResultButton;
@property NSString *response;


@end

@implementation EnterRepetitionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self.speech isEqualToString:[self.speech uppercaseString]]) {
        self.response = @"NO. WE CAN'T DO THAT.";
        self.responseLabel.text = self.response;
    } else {
        self.response = @"HUH?! SPEAK UP, SANDRA!";
        self.responseLabel.text = self.response;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Result"]) {
        ResultViewController *resultViewController = [segue destinationViewController];
        
        resultViewController.numberOfRepeat = [self.timeToRepeatField.text integerValue];
        resultViewController.response = self.response;
    } else if ([segue.identifier isEqualToString:@"Randomize"]) {
        ResultViewController *resultViewController = [segue destinationViewController];
        
        resultViewController.numberOfRepeat = arc4random_uniform(74);
        resultViewController.response = self.response;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
