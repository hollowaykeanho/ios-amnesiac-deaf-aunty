//
//  ResultViewController.m
//  AmnesiacDeafAunty
//
//  Created by Kean Ho Chew on 28/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;


@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    int i = 0;
    NSMutableString *final_response = [[NSMutableString alloc] initWithCapacity:50];
    
    for(i=0; i<= self.numberOfRepeat; i++) {
        [final_response appendString:self.response];
        [final_response appendString:@"\n"];
    }
    
    self.textView.text = final_response;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
