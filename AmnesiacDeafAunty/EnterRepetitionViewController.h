//
//  EnterRepetitionViewController.h
//  AmnesiacDeafAunty
//
//  Created by Kean Ho Chew on 28/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterRepetitionViewController : UIViewController

@property NSString *speech;

@end
