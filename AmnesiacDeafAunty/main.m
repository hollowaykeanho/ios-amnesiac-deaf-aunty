//
//  main.m
//  AmnesiacDeafAunty
//
//  Created by Kean Ho Chew on 28/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
